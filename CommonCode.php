<?php

========================================== GETTING NAV ITEMS

?>
<ul class="primary">
	<?php
	$nav_items = wp_get_nav_menu_items("Main");

	foreach($nav_items as $k => $item)
	{
	    ?>
	    <li>
	        <a href="<?php echo $item->url;?>"><?php echo $item->title;?></a>
	    </li>
	    <?php
	}
	?>
</ul>
<?php

========================================== GETTING FEATURED IMAGE

$image = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), "full_screen");


========================================== GETTING CUSTOM TAXONOMIES ATTACHED TO POST

$terms = wp_get_post_terms($post->ID,"client");


========================================== EASY FEATURED CONTENT: SUPPORT

add_post_type_support('my-post-type-slug', 'featured-content');
remove_post_type_support('page', 'featured-content');