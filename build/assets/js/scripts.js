var TeethObject = function(args) {
  this.config = args;
};

TeethObject.prototype.init = function() {
  // The setup is run once
  this.setup();

  // The fire method is fired after every ajax call
  this.fire();
};

/**
 * The setup method is called on the initial load
 * and sets up any first time variables that get
 * used subsequently through the site
 */
TeethObject.prototype.setup = function() {
  this._setVariables();
  this._setupHistory();
  this._setupHeadroom();
  this._singleEvents();
  this._checkSplash();
  this._searchInit();
};
  
  /**
   * @private
   *
   * The _setVariables method sets up the variables
   * that get used consisttently through the site
   */
  TeethObject.prototype._setVariables = function() {

    // This object holds all the properties needed for the ajax calls
    this.ajax = {
      // Path is incremented every ajax call
      path: 0,
      // This is the current state object
      old: this.config.state,
      // New inherits the new state object when a call is fired
      new: {},
      // Timestamp of the start of the ajax call
      start: 0,
      // This increments when:
      //  1) the data is loaded in an ajax call
      //  2) the fade out transitions have finished
      flag: 0,
      // A property to track whether its transitioning
      transitioning: false
    };

    // The length of time splash is displayed
    this.splashDelay = 2000;

    // Caches the transition event with vendor prefix
    this.transitionString = this.whichTransitionEvent();

    // The class that is used for the actor elements
    this.actorCls = '.actor';
    
    // Caches the variables that are used in the requestAnimationFrame
    this.scrollVars = {
      y: 0,
      ticking: false
    };

    // This object holds booleans of whether certain methods
    // need to get fired
    this.animationCache = {
      articleFollow: false
    };

    this.article = new TeethArticle(this);

    // The mobile menu variable
    this.menu = false;
  };
  
  /**
   * @private
   *
   * Reset variables, does kind of what it sounds like
   */
  TeethObject.prototype._resetVariables = function() {
    this._articleTrans = false;
    this._articlesFinished = false;
    this._articleAjax = false;

    this.stockists = null;

    if($(".locations").length) {
      this.stockists = new TeethStockists(this);
    }
  };

  /**
   * @private
   *
   * Sets up the binding for the history handling
   */
  TeethObject.prototype._setupHistory = function() {
    window.onpopstate = this._manageHistory.bind(this)
  };

  /**
   * @private
   *
   * The method that is called when pop state is fired.
   * This handles refiring the ajax calls for the previous content
   * in the history array
   */
  TeethObject.prototype._manageHistory = function(e) {
    // Manually sets the window title from the state object
    document.title = this._generateTitle(e.state);

    // Fire the content method from the newly popped state
    this._fireContent(e.state);
  };

  /**
   * @private
   *
   * This is the important method that takes a state object
   * and from that initiates the ajaxCall for the new page
   *
   * @param {Object:State}
   */
  TeethObject.prototype._fireContent = function(state) {
    // Transitioning begins here
    this.ajax.transitioning = true;

    // Sets the called state as the new state object
    this.ajax.new = state;

    // Adds some extra parameters for the actual php ajax call
    this.ajax.new.action = 'ajaxReplace';
    this.ajax.new.nonce = this.config.site.nonce;
    this.ajax.new.path = this.ajax.path;

    // Starts the timestamp at this current point
    this.ajax.start = this.timeNow();
    // Sets the initial timestamp flag to be the same as the start
    this.ajax.flag = this.ajax.start;

    // Fires in parallel getting the content and starting the
    // actual visual transition
    this._getContent();
    this._transOut();
  };

    /**
     * @private
     *
     * Fires the actual ajax call for the new content
     */
    TeethObject.prototype._getContent = function() {
      $.ajax({
        dataType: "json",
        url: this.config.urls.ajax,
        data: this.ajax.new,
        success: this._evaluateContent.bind(this)
      });
    };

    /**
     * @private
     *
     * This method gets called on the success from the ajax object
     * @param {Object} data The response from the ajax call
     */
    TeethObject.prototype._evaluateContent = function(data) {
      // If a timer was set (because the transition hasnt finished)
      // clear it first just in case
      clearTimeout(this.ajax.timer);

      // An extra check to see if this data returned is the data we
      // actually want and not an old ajax call
      if(data.query.path == this.ajax.path) {

        // If the flag no longer matches the start, the transition has finished
        if (this.ajax.start != this.ajax.flag) {
          this.config.state = this.ajax.new;
          this._replaceContent(data);
        }
        else {
          // Refire this exact method if the transition hasnt finished
          this.ajax.timer = setTimeout(function() {
            this._evaluateContent(data);
          }.bind(this), 50);
        }
      }
    };

    /**
     * @private
     *
     * Transitions the actor elements out and calls
     * the event attacher to figure out the end of the transition
     */
    TeethObject.prototype._transOut = function() {
      // Actors get the general transitioning rule added
      $(this.actorCls).addClass("trans--rule");

      // Reset menu
      this._toggleMenu(false);

      // Adds this 1 frame later just so the events get registered correct
      this.async(function() {
        this._actorsFinished(this._bookendTransOut.bind(this));

        // Actors get the transition out class
        $(this.actorCls).addClass("trans--out");
      }.bind(this), 1);
    };

    /**
     * @private
     *
     * Sets the flag when the transition out has finished
     * so the ajax content call can get notified
     */
    TeethObject.prototype._bookendTransOut = function() {
      this.ajax.flag = this.timeNow();
    };

    /**
     * @private
     *
     * This method takes the data from the successful ajax
     * call and replaces the content in the dom. It also 
     * alters certain classes and structural things
     */
    TeethObject.prototype._replaceContent = function(data) {
      // If the content returned is a dark page, switch the classes
      if(data.arguments.structure.dark) {
        $("html").addClass("site--dark");
      }
      else {
        $("html").removeClass("site--dark");
      }

      // Change data type so we could target their styles
      $("html").attr("data-type",data.arguments.state.type);
      
      // Replaces the content in the DOM
      $("#ajax-replace").html(data.content);

      // Remove all the active nav items classes
      $(".nav-items__item__link--active").removeClass("nav-items__item__link--active");

      // If there is a new nav active item add the class here
      if(data.arguments.structure.active) {
        $(".nav-items__item__link--" + data.arguments.structure.active).addClass("nav-items__item__link--active");
      }

      // Make sure its scrolled back to the top
      $("html,body").scrollTop(0);

      // Fire the fire method with the new content
      this.fire();

      // Fires the transition in method
      this._transIn();
    };

    /**
     * @private
     *
     * Essentially the flip of the transOut
     */
    TeethObject.prototype._transIn = function() {
      // Temporarily remove the transition rule so we can
      // safely remove the transition out rule
      $(this.actorCls).removeClass("trans--rule");
      this.async(function() {
        $(this.actorCls).removeClass("trans--out");
      }.bind(this), 1);

      // Handle the transition in shit
      this.async(function() {
        $(this.actorCls).addClass("trans--rule");
        this._actorsFinished(this._bookendTransIn.bind(this));
        $(this.actorCls).removeClass("trans--in");
      }.bind(this), 2);
    };

    /**
     * @private
     *
     * This method cleans up the transition in
     * portion of the ajax method
     */
    TeethObject.prototype._bookendTransIn = function() {
      this.ajax.transitioning = false;
      this.ajax.flag = this.timeNow();
    };

    /**
     * @private
     *
     * Adds event listeners for the transition end
     * on the actor elements and fires a callback
     * when theyre all finished
     * @param {Function}fnc The callback function
     */
    TeethObject.prototype._actorsFinished = function(fnc) {
      var len = $(this.actorCls).length,
          local = 0,
          self = this;

      $(this.actorCls).on(this.transitionString, function() {
        local++;

        if (local == len) {
          fnc();
        }
        $(this).off(self.transitionString);
      });
    }

  /**
   * @private
   *
   * Sets up the whole html tag as a headroom
   * element so that any children can benefit from
   * the headroom classes
   */
  TeethObject.prototype._setupHeadroom = function() {
    $("html").headroom({
      // vertical offset in px before element is first unpinned
      offset : 200,
      // scroll tolerance in px before state changes
      tolerance : 0,
      // or you can specify tolerance individually for up/down scroll
      tolerance : {
          up : 5,
          down : 0
      },
      // css classes to apply
      classes : {
          // when element is initialised
          initial : "bite",
          // when scrolling up
          pinned : "bite--off",
          // when scrolling down
          unpinned : "bite--on",
          // when above offset
          //top : "bite--na",
          // when below offset
          notTop : "bite--not-top",
          // when at bottom of scoll area
          bottom : "bite--bottom",
          // when not at bottom of scroll area
          notBottom : "bite--not-bottom"
      }
    });
  };

  TeethObject.prototype._parallaxInit = function(el) {

       var el = $(el).find(".advert-content"),
         el_top = el.offset().top,
         el_data_diff = ((window.scrollY - el_top) * -0.05);

        el.attr('data-top', el_top);

  };

  TeethObject.prototype._parallax = function() {
    if ($(".advert--full.advert--loaded").length) {

      $(".advert--full.advert--loaded").each( function() {

        var el = $(this).find(".advert-content");

        var windH = $(window).height();

        var el_top = el.offset().top,
          el_data_top = el.data("top"),
          el_bottom = el_top + el.height(),
          scroll_bottom = window.scrollY + windH,
          translate_val = (window.scrollY - el_data_top) * -0.1;

          if (el.hasClass("advert-content--video")) {
            $(this).find("video").css({
              top: translate_val
            });
          } else {
            el.css({
              'background-position': 'center ' + translate_val + 'px',
            });            
          }          

      });
    }
  };  
  
  TeethObject.prototype._toggleMenu = function(bool)
  {
    var self = this;

    if(typeof bool !== "undefined")
    {
      this.menu = bool ? false : true;
    }

    if(this.menu)
    {
      this.menu = false;
      $("html").removeClass("mobile-open");
    }
    else
    {
      this.menu = true;
      $("html").addClass("mobile-open");
    }
  }

  TeethObject.prototype._checkSplash = function() {
    // If the splash element exists
    if ($(".splash").length) {
      this.splashTimer = setTimeout(this._removeSplash.bind(this), this.splashDelay);
      $(".splash").click(this._removeSplash.bind(this));
    }
  };

  TeethObject.prototype._removeSplash = function() {
    clearTimeout(this.splashTimer);

    $(".splash").fadeOut(500, function() {
      $(this).remove();
    });
  };

  TeethObject.prototype._searchInit = function() {
    this.search = new TeethSearch(this);
  };

  /**
   * @private
   *
   * This method sets any events that only need to get
   * set once on initial load
   */
  TeethObject.prototype._singleEvents = function() {
    var self = this;

    $(window).scroll(function(e) {
      self.scrollVars.y = window.scrollY;
      self._requestScrollTick();
    });

    // Bind the navicon to open the mobile menu
    $(".open-mobile-nav").unbind("click");
    $(".open-mobile-nav").click(function(e) {
      e.preventDefault();
      self._toggleMenu();
      return false;
    });    
  };

    /**
     * @private
     *
     * A method that allows the debouncing of the scroll
     * event, so the call stack doesn't get used up
     */ 
    TeethObject.prototype._requestScrollTick = function() {
      // If ticking is false that means the animation loop method
      // is freed up and ready to be called again
      if (!this.scrollVars.ticking) {
        requestAnimationFrame(this._animationLoop.bind(this));
      }

      this.scrollVars.ticking = true;
    };

    /**
     * @private
     *
     * Animation loop method that gets called from
     * requestAnimationFrame. This method does nothing
     * itself, but fires other methods if they're needed
     * on the page
     */
    TeethObject.prototype._animationLoop = function() {
      // If ajax is transitioning do not call any methods
      if(!this.ajax.transitioning) {

        // If we're on a article page, fire the article methods
        if (this.animationCache.articleFollow) {
          this._articleFollow();
        }

        this._lazyImage();
        this._parallax();
      }

      this.scrollVars.ticking = false;
    };

    TeethObject.prototype._articleFollow = function() {
      this.article.loop();
    };

    TeethObject.prototype._lazyImage = function() {
      var windH = $(window).height();
      var self = this;

      $(".lazy-image").not(".lazy-image--loading, .lazy-image--loaded").each(function(i,e) {
        var rect = this.getBoundingClientRect();

        if(rect.top > 0 && rect.top < windH) {
          self._lazyLoadImage(this);
        }
      });
    };

    TeethObject.prototype._lazyLoadImage = function(el) {
      $(el).addClass("lazy-image--loading");

      var url = $(el).data("url"),
          self = this;

      var img = new Image();
      img.src = url;
      img.onload = function() {
        $(el).find("img").attr("src", url);
        $(el).removeClass("lazy-image--loading");

        self.async(function() {
          $(el).addClass("lazy-image--loaded");
        }, 500);
      };
    };


TeethObject.prototype.fire = function() {
  this.ajax.path++;

  this._resetVariables();
  this._events();
  this._updateAnimationCache();
  this._shareButtons();
  this._lazyImage();

  this.async(function() {
    this._loadAdverts();
  }.bind(this), 100);
};
  
  TeethObject.prototype._events = function() {
    $("[data-ajax]").not(".ajax--bound").click(this._ajaxClick.bind(this));
    $("[data-ajax]").not(".ajax--bound").addClass("ajax--bound");

    $(".load-more .btn").click(this._loadMoreClick.bind(this));

    $(".signup__button").unbind('click');
    $(".signup__button").click(this._signupClick.bind(this));

    $(".signup__input").unbind('keyup');
    $(".signup__input").keyup(this._signupKeyup.bind(this));
  };

  TeethObject.prototype._ajaxClick = function(e) {
    if (e.button === 0) {
      e.preventDefault();

      if(!this.ajax.transitioning) {
        var stateObj = $(e.delegateTarget).data("state");

        this.commitState(stateObj);
      }
    }
  };

  TeethObject.prototype.commitState = function(state) {
    if(this.config.state != state) {
      history.pushState(state, this._generateTitle(state), this.config.urls.site + state.slug);
      document.title = this._generateTitle(state);

      this._fireContent(state);
    }
  }

  TeethObject.prototype._loadMoreClick = function(e) {
    var obj     = this.config.state;
    obj.nonce   = this.config.site.nonce;
    obj.action  = 'loadMore';
    
    var newObj = $.extend(obj, $(e.delegateTarget).data());

    $(".load-more .btn").addClass("btn--loading");

    $.ajax({
      dataType: "json",
      url: this.config.urls.ajax,
      data: obj,
      success: this._evaluateLoadMore.bind(this)
    });
  };

    TeethObject.prototype._evaluateLoadMore = function(data) {
      
      if(data.arguments.morePosts) {
        $(".load-more .btn").data("page", parseInt(data.arguments.loadPage) + 1);
      }
      else {
        $(".load-more").slideUp(250);
      }
      
      $(".home-container").last().append(data.content);

      this.async(function() {
        $(".post-item--hidden").removeClass("post-item--hidden");
      }.bind(this), 1);
    };

  TeethObject.prototype._updateAnimationCache = function() {

    this.animationCache.articleFollow = $(".article--not-featured").length ? true : false;
  };

  TeethObject.prototype._loadAdverts = function() {
    var self = this;

    $(".advert").not(".advert--loaded").each(function(i,e) {
      self._loadSingleAdvert(this);
    });
  };

  TeethObject.prototype._loadSingleAdvert = function(el) {
    var self        = this;

    var obj         = {};
    obj.nonce       = this.config.site.nonce;
    obj.action      = 'getAdvert';
    obj.size        = $(el).data("size");
    obj.placement   = $(el).data("placement");    

    $.ajax({
      dataType: "json",
      url: this.config.urls.ajax,
      data: obj,
      success: function(data) {

        if(data.noAdvert) {
          $(el).slideUp(250);
        }

        else {

          $(el).html(data.content);

          self.async(function() {
            var cont = $(el).find(".advert-content");
            cont.removeClass("advert-content--hidden");
            $(el).addClass("advert--loaded");
            if ($(el).hasClass("advert--full")) {

              //preload video
              var vid_src = cont.attr("data-vid");

              if (cont.hasClass("advert-content--video")) {

                var vid_block = '<video preload="auto" muted loop>'+
                                  '<source src="'+vid_src+'" type="video/mp4">'+
                                  'Your browser does not support the video tag.'+
                                '</video>';
                var vid = $.parseHTML(vid_block);

                $(el).find(".advert-content").append(vid);

                //keep checking whether the video is ready
                this.loadTimer = setInterval( function() {

                  var vid = cont.find("video");

                  if (vid.prop('readyState') === 4) {
                    
                    //play video when it's ready
                    if (vid.get(0).paused) {
                      vid.get(0).play();  
                    }

                    //stop checking if loaded
                    clearInterval(this.loadTimer);

                  }

                }, 20);

              }

              //init parallax
              self._parallaxInit(el);

            }

          }, 20);

        }

      }

    });
  };

  TeethObject.prototype._signupKeyup = function(e) {
    if(e.keyCode === 13) {
      this._signupClick(e);
    }
  };

  TeethObject.prototype._signupClick = function(e) {
    var parentSignup  = $(e.target).parents(".signup");

    if(!parentSignup.hasClass("signup--disabled")) {
      $(".signup").addClass("signup--disabled");

      var inputSignup   = parentSignup.find(".signup__input"),
          emailVal      = inputSignup.val();

      if(this.validateEmail(inputSignup.val())) {
        this._fireSignup(emailVal);
      }
      else {
        this._signupMessage("error", "Not a valid email");
        $(".signup").removeClass("signup--disabled");
      }
    }
  };

  TeethObject.prototype._fireSignup = function(val) {
    var self    = this;

    var obj     = {};
    obj.nonce   = this.config.site.nonce;
    obj.action  = 'addEmail';
    obj.email   = val;

    $.ajax({
      dataType: "json",
      url: this.config.urls.ajax,
      data: obj,
      success: this._signupValidate.bind(this)
    });
  };

  TeethObject.prototype._signupMessage = function(status, msg) {
    clearTimeout(this.signupTimer);

    $(".signup").addClass("signup--msg-"+status + " signup--msg-show");
    $(".signup__message").text(msg);

    this.signupTimer = setTimeout(function() {
      $(".signup").removeClass("signup--msg-"+status+" signup--msg-show");
      $(".signup__message").text("");
    }, 2000);
  };

  TeethObject.prototype._signupValidate = function(data) {
    if(data.success) {
      this._signupMessage('success', data.message);
    }
    else {
      this._signupMessage('error', data.message);
    }
    $(".signup").removeClass("signup--disabled");
  };

  TeethObject.prototype._shareButtons = function() {
    var self = this;

    $(".share-link").not(".event--bound").click(function(e) {
      e.preventDefault();

      if(!$(this).hasClass("share-link--pinterest")) {
        self.popupCenter($(this).attr("href"), "Share window", 400, 250);
      }
      else {
        self.pinit();
      }
    });

    $(".share-link").addClass("event--bound");
  };

TeethObject.prototype._generateTitle = function(obj) {
  return obj.title + " \u2014 " + this.config.site.name;
};

var app = new TeethObject(args); // Args declared in header.php

$(document).ready(function() {
  app.init();
});
var TeethArticle = function(app) {
  this.app = app;
};

TeethArticle.prototype.loop = function() {
  var self = this,
      windH = $(window).height(),
      scrollT = $(window).scrollTop(),
      elemVars = {
        perc: 0,
        elem: null,
        id: 0,
        name: '',
        key: -1
      },
      navH = $("nav").height();

  var tempVars = false;

  $(".article--not-featured").each(function(i, e) {


    tempVars = self._calculatePosition(this, windH, scrollT);

    var item = $(".post-sidebar__item--" + tempVars.id),
        bar = item.find(".post-sidebar__link__percentage__bar"),
        left = $(this).find(".article__left");

    if (tempVars.perc < 0) {
      $(this).addClass("article--future");
      $(this).removeClass("article--present article--past");

      if(!bar.hasClass('zero')) {
        bar.addClass('zero');
        bar.css({
          'transform': 'scale(1, 0)'
        });
      }
    }
    else if (tempVars.perc > 1) {
      $(this).addClass("article--past");
      $(this).removeClass("article--future article--present");

      if(!bar.hasClass('complete')) {
        bar.addClass('complete');
        bar.removeClass('zero');
        bar.css({
          'transform': 'scale(1, 1)'
        });
      }
    }
    else {
      if(tempVars.btm - (windH / 2) > 0) {

        if(tempVars.top - ((windH / 2) - navH) > 0) {
          $(this).addClass("article--present");
          $(this).removeClass("article--future article--past");
        }
        else {
          $(this).addClass("article--future");
          $(this).removeClass("article--present article--past");
        }
      }
      else {
        $(this).addClass("article--past");
        $(this).removeClass("article--future article--present");
      }

      bar.removeClass('complete zero');

      if(!item.hasClass("post-sidebar__item--active")) {
        $(".post-sidebar__item--active").removeClass("post-sidebar__item--active");
        item.addClass("post-sidebar__item--active");

        var itemRect = item.get(0).getBoundingClientRect();

        // $(".sidebar").animate({
        //   scrollTop : $(".sidebar").get(0).scrollTop + itemRect.top - $(".nav-bar--top").height()
        // }, 250);

        $(".sidebar").animate({
          scrollTop : $(".sidebar").get(0).scrollTop + itemRect.top
        }, 250);

        var stateObj = item.find(".post-sidebar__link").data("state");

        if(window.location.pathname.indexOf(stateObj.slug) < 0) {
          history.replaceState(stateObj, self.app._generateTitle(stateObj), self.app.config.urls.site + stateObj.slug);
          document.title = self.app._generateTitle(stateObj);
        }
      }

      bar.css({
        'transform': 'scale(1, '+ tempVars.perc +')'
      });
    }
  });

  if(!this._articlesFinished) {
    elemVars = self._calculatePosition($(".article--not-featured").last().get(0), windH, scrollT);


    if(elemVars.perc > 0.2) {
      var nextLink = $(".post-sidebar__item--" + elemVars.id).next().find(".post-sidebar__link");

      if(nextLink.length) {
        this._addArticleOn(nextLink.data("state"));
      }
      else {
        this._articlesFinished = true;
        // Reveal footer
        $("footer").addClass("visible");
      }
    }
  }

  var footerRect = (this._articlesFinished) ? $("footer").get(0).getBoundingClientRect() : 0 ;

  var pixelsOn = footerRect.top - windH;

  if(pixelsOn < 0) {
    $(".sidebar-fixed").css({
      transform: "translateY(" + pixelsOn + "px)"
    });
  }
  else {
    $(".sidebar-fixed").css({
      transform: "translateY(0px)"
    });
  }
};

TeethArticle.prototype._calculatePosition = function(el, windH, scrollT) {
  var elem = $(el);

  var elemPos = el.getBoundingClientRect();

  var perc = this.app.mapRange((elemPos.top - (windH / 2)), 0, (elemPos.height * -1), 0, 1);

  var pixels = perc * elemPos.height;

  var obj = {
    el    : el,
    perc  : perc,
    btm   : elemPos.height - pixels,
    top   : pixels,
    id    : elem.data('id'),
    name  : elem.data('name')
  };

  return obj;
};

TeethArticle.prototype._addArticleOn = function(state) {

  if(!this.app._articleTrans) {
    this.app._articleTrans = true;

    this.app._articleAjax = state;
    this.app._articleAjax.action = 'nextArticle';
    this.app._articleAjax.nonce = this.app.config.site.nonce;

    $.ajax({
      dataType: "json",
      url: this.app.config.urls.ajax,
      data: this.app._articleAjax,
      success: this._renderArticleOn.bind(this)
    });
  }
};

TeethArticle.prototype._renderArticleOn = function(data) {
  var self = this;

  $(".article--not-featured").last().after(data.content);

  this.app.async(function() {
    $(".article--not-featured").last().find(".actor").removeClass("trans--in");
    self.app.fire();
  }, 5);
};
var TeethSearch = function(app) {
	this.app = app;

	this.opened = false;
	this.el = $(".search");
	this.state = this.app.config.searchState;

	this.init();
};

TeethSearch.prototype.init = function() {
	this.el.find(".search__icon").click(this.btnClick.bind(this));
	this.el.find(".search__input").keyup(this.keyUp.bind(this));
};

TeethSearch.prototype.btnClick = function() {
	if (this.opened) {
		this.search();
	}
	else {
		this.open();
	}
};

TeethSearch.prototype.keyUp = function(e) {
	if (e.keyCode === 13) {
		this.search();
	}

	if (e.keyCode === 27) {
		this.close();
	}
};

TeethSearch.prototype.open = function() {
	this.opened = true;
	this.el.addClass("search--open");

	this.el.find(".search__input").focus();

	this.app.async(function() {
		$(document).click(this.shouldClose.bind(this));
	}.bind(this), 1);
};

TeethSearch.prototype.close = function() {
	this.opened = false;
	this.el.removeClass("search--open");

	this.el.find(".search__input").blur();

	$(document).unbind("click");
};

TeethSearch.prototype.shouldClose = function(e) {
	if ($(e.target).hasClass("search__input") ||
		$(e.target).hasClass("search__icon")) {
		return false;
	}
	else {
		this.close();
	}
};

TeethSearch.prototype.search = function() {
	var searchEl 	= this.el.find(".search__input"),
		val 		= searchEl.val();

	if(val.trim() !== '') {
		var state 		= this.state;
		state.absolute 	= this.app.config.urls.site + "?/search/q=" + encodeURI(val);
		state.slug 		= "/search/?q=" + encodeURI(val);
		state.search 	= val;

		history.pushState(state, this.app._generateTitle(state), this.app.config.urls.site + state.slug);
	    document.title = this.app._generateTitle(state);

	    this.app._fireContent(state);

	}

	this.close();
};

var TeethStockists = function(app) {
  this.app = app;
  this.overlay = $(".locations");
  this.region = '';

  this.init();
};

TeethStockists.prototype.init = function() {
  $("#stockistOpen").click(this.open.bind(this));

  this.overlay.click(this.shouldClose.bind(this));

  this.overlay.find("a").click(this.linkRegion.bind(this));

  this.alreadySet();
};

TeethStockists.prototype.alreadySet = function() {
	var params = this.app.queryString();
  if(params.hasOwnProperty('region')) {
    this.setRegion(params.region);
  }
};

TeethStockists.prototype.open = function() {
  this.overlay.addClass("locations--show");
  $("html").css({overflow: "hidden"});
};

TeethStockists.prototype.close = function() {
  this.overlay.removeClass("locations--show");
  $("html").removeAttr("style");
};

TeethStockists.prototype.shouldClose = function(e) {
	if (e.target.tagName === 'LI' ||
		e.target.tagName === 'UL' ||
		e.target.tagName === 'A') {
		return false;
	}
	else {
		this.close();
	}
};

TeethStockists.prototype.linkRegion = function(e) {
	e.preventDefault();

	var curr = $(e.currentTarget);

	var state = this.app.config.state;
	state.absolute = curr.attr("href");
	state.slug = '/stockists/?region=' + curr.data("region")

	history.replaceState(this.app.config.state, this.app._generateTitle(this.app.config.state), curr.attr("href"));

  this.close();
  this.setRegion(curr.data("region"));
};

TeethStockists.prototype.setRegion = function(region) {
  if(this.region !== '') {
    $(".stockist").not("."+this.region).removeClass("stockist--hide");
  }

  if(region !== '') {
    $(".stockist").not("."+region).addClass("stockist--hide");
    var title = $('a[data-region="'+region+'"').html();    
  } else {
    var title = "Choose a Region";    
  }

  $(".page-container__region-select button span").html(title);

  this.region = region;
};



//https://davidwalsh.name/javascript-debounce-function
TeethObject.prototype.debounce = function(func, wait, immediate) {
  var timeout;
  return function() {
    var context = this, args = arguments;
    var later = function() {
      timeout = null;
      if (!immediate) func.apply(context, args);
    };
    var callNow = immediate && !timeout;
    clearTimeout(timeout);
    timeout = setTimeout(later, wait);
    if (callNow) func.apply(context, args);
  };
};


TeethObject.prototype.timeNow = function() {
  return new Date().getTime();
}

/* From Modernizr */
TeethObject.prototype.whichTransitionEvent = function(){
    var t;
    var el = document.createElement('fakeelement');
    var transitions = {
      'transition':'transitionend',
      'OTransition':'oTransitionEnd',
      'MozTransition':'transitionend',
      'WebkitTransition':'webkitTransitionEnd'
    }

    for(t in transitions){
        if( el.style[t] !== undefined ){
            return transitions[t];
        }
    }
}

TeethObject.prototype.async = function(fnc, t) {
  return setTimeout(fnc, t);
};

TeethObject.prototype.mapRange = function(value, low1, high1, low2, high2) {
    return low2 + (high2 - low2) * (value - low1) / (high1 - low1);
};

TeethObject.prototype.validateEmail = function(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

TeethObject.prototype.popupCenter = function(url, title, w, h) {
    // Fixes dual-screen position                         Most browsers      Firefox
    var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;
    var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;

    var width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
    var height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

    var left = ((width / 2) - (w / 2)) + dualScreenLeft;
    var top = ((height / 2) - (h / 2)) + dualScreenTop;
    var newWindow = window.open(url, title, 'scrollbars=yes, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);

    // Puts focus on the newWindow
    if (window.focus) {
        newWindow.focus();
    }
}

TeethObject.prototype.pinit = function() {
  var e = document.createElement('script');
  e.setAttribute('type','text/javascript');
  e.setAttribute('charset','UTF-8');
  e.setAttribute('src','//assets.pinterest.com/js/pinmarklet.js?r='+Math.random()*99999999);
  document.body.appendChild(e);
};


// http://stackoverflow.com/a/979995
TeethObject.prototype.queryString = function () {
  // This function is anonymous, is executed immediately and
  // the return value is assigned to QueryString!
  var query_string = {};
  var query = window.location.search.substring(1);
  var vars = query.split("&");
  for (var i=0;i<vars.length;i++) {
    var pair = vars[i].split("=");
        // If first entry with this name
    if (typeof query_string[pair[0]] === "undefined") {
      query_string[pair[0]] = decodeURIComponent(pair[1]);
        // If second entry with this name
    } else if (typeof query_string[pair[0]] === "string") {
      var arr = [ query_string[pair[0]],decodeURIComponent(pair[1]) ];
      query_string[pair[0]] = arr;
        // If third or later entry with this name
    } else {
      query_string[pair[0]].push(decodeURIComponent(pair[1]));
    }
  } 
  return query_string;
};

TeethObject.prototype.capitalizeFirstLetter = function(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}


//# sourceMappingURL=scripts.js.map
