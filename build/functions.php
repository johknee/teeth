<?php
require_once "vendor/autoload.php";

require_once "includes/funcHelp.php";

spl_autoload_register(function ($class_name) {
    include get_template_directory(). "/includes/classes/" . $class_name . '.php';
});

//--------------------------------------|~~~|
//
//   POST TYPES STAGING AREA
//
//-------------------------------------------
	
	/*
	This is where you can easily register any custom post types.
	There is a helper function inside of funcHelp.php, that you shouldn't 
	need to touch at all, but that takes the array below and runs it through
	wordpress and registers it as a custom post type.
	
	This is a break down of the array to register post types.

	array(
		"plural"	=> "Post_types",      /~/-- If your post type was 'Person' this field you would put 'People'
		"single"	=> "Post_type",       /~/-- Here you would put whatever you want your custom post type
		"icon"		=> "dashicons-menu"   /~/-- Here you put the name of the icon you want to display for your post type
	),	                                     -- You can find all the icons here https://developer.wordpress.org/resource/dashicons/
	
	You can put as many of these arrays in here as you like, and the helper
	function will iterate over all of them and create them.

	In addition to that you can actually add more values to the array, to
	override any of the default arguments that create the post type with
	`register_post_type`.

	So for example, if you wanted to create a post type called 'Person' that
	only supported a title and an image, no content, your array might look
	like this:

	array(
		"plural"	=> "People",
		"single"	=> "Person",
		"icon"		=> "dashicons-groups",
		"supports" 	=> array('title','thumbnail')
	),
	*/
	

	$my_post_types = array(
		array(
			"plural"	=> "Adverts",
			"single"	=> "Advert",
			"icon"		=> "dashicons-tickets"
		),
		array(
			"plural"	=> "Stockists",
			"single"	=> "Stockist",
			"icon"		=> "dashicons-cart",
			"supports"=> array("title")
		),
	);
	
	add_action('init', 'createPostTypes');
	
	/*
	This is where you can easily create custom taxonomies for existing post
	types or the custom post types that you just created.

	The one thing to essentially remember with this, is that in the post_type
	field, you want to put the lower case 'slugified' version of the post type.
	So if your post type is 'Big Data', you'd put in here 'big_data'

	Same goes with this taxonomy helper, that you can actually add any values
	in and it will override the default arguments for the wordpress function
	`register_taxonomy`
	*/

	$my_taxonomies = array(
		array(
			"plural"	=>"Regions",
			"single"	=>"Region",
			"post_type"	=>"stockist"
		),
	);
	
	add_action('init', 'createTaxonomies');


//--------------------------------------|~~~|
//
//   IMAGE SIZE STAGING AREA
//
//-------------------------------------------
	
	/*
	Nothing special here, just add any image sizes you might need for the site
	to generate here. You can put in any other functions to help with the
	theme and it will hook in to wordpress' life cycle and run properly too.
	*/

	function theme_setup() {
		add_image_size('full_screen', 1920, 1080, true);
		add_image_size('featured', 960, 0, true);
	}
	add_action( 'after_setup_theme', 'theme_setup' );


// The standard ajax to get actual pages
add_action("wp_ajax_ajaxReplace", "ajaxReplace");
add_action("wp_ajax_nopriv_ajaxReplace", "ajaxReplace");

function ajaxReplace() {
	if (!wp_verify_nonce($_REQUEST['nonce'], "ajaxHandlerNonce")) {
		exit("No naughty business please");
	}

	$ret = (object) array();

	switch($_GET['type']) {
		case "page":
			$post = get_post($_GET['id']);

			if($post->post_title === "Search") {
				$obj = new TeethPage($post);
				$ret = $obj->init(false, [
					'search' => $_GET['search']
				]);
			}
			else {
				$obj = new TeethPage($post);
				$ret = $obj->init();
			}
			break;
		case "category":
			$post = get_category($_GET['id']);
			$obj = new TeethCategory($post);
			$ret = $obj->init();
			break;
		case "post":
			$post = get_post($_GET['id']);
			$obj = new TeethPost($post);
			$ret = $obj->init();
			break;
	}

	$ret->query = (object) $_GET;

	echo json_encode($ret);

	die();
}

// Get the next article
add_action("wp_ajax_nextArticle", "nextArticle");
add_action("wp_ajax_nopriv_nextArticle", "nextArticle");

function nextArticle() {
	if (!wp_verify_nonce($_REQUEST['nonce'], "ajaxHandlerNonce")) {
		exit("No naughty business please");
	}

	$post = get_post($_GET['id']);
	$obj = new TeethPost($post);
	$obj->setSingle();
	$ret = $obj->init();

	echo json_encode($ret);

	die();
}

// Load more articles
add_action("wp_ajax_loadMore", "loadMore");
add_action("wp_ajax_nopriv_loadMore", "loadMore");

function loadMore() {
	if (!wp_verify_nonce($_REQUEST['nonce'], "ajaxHandlerNonce")) {
		exit("No naughty business please");
	}

	switch($_GET['type']) {
		case "page":
			$post = get_post($_GET['id']);
			$obj = new TeethPage($post);
			break;
		case "category":
			$post = get_category($_GET['id']);
			$obj = new TeethCategory($post);
			break;
	}

	$obj->setLoadMore();
	$args = [];

	foreach($_GET as $k => $v) {
		switch($k) {	
			case "postNotIn":
				$args['post__not_in'] = [$v];
				break;
			case "paged":
				$args['page'] = $v;
				break;	
			default:
				$args[$k] = $v;
				break;
		}
	}
	
	$ret = $obj->init(false, $args);

	echo json_encode($ret);

	die();
}

// Load an advert
add_action("wp_ajax_getAdvert", "getAdvert");
add_action("wp_ajax_nopriv_getAdvert", "getAdvert");

function getAdvert() {
	if (!wp_verify_nonce($_REQUEST['nonce'], "ajaxHandlerNonce")) {
		exit("No naughty business please");
	}

	$advert = new TeethAdvert($_GET['size'],$_GET['placement']);
	$ret = $advert->render();

	echo json_encode($ret);

	die();
}

// Put this on hold for now
// function wrapImagesInDiv($content) {
//    $pattern = '/(<img[^>]*class=\"([^>]*?)\"[^>]*>)/i';
//    $replacement = '<div class="lazy-image" data-url="'.$url.'">' . 
// 	                '<img class="lazy-image__image" src="http://dummyimage.com/'.$wid.'x'.$hei.'/'.$color.'/'.$color.'">'.
// 	              '</div>';
//    $content = preg_replace($pattern, $replacement, $content);
//    return $content;
// }
// add_filter('the_content', 'wrapImagesInDiv');



/*
 * Remove links from new images
 */

add_action('admin_init', 'iloveitaly_imagelink_setup', 10);
function iloveitaly_imagelink_setup() {
	$image_set = get_option( 'image_default_link_type' );
	
	if ($image_set !== 'none') {
		update_option('image_default_link_type', 'none');
	}
}



/*
 * Remove links from exisiting images
 */

add_filter('the_content', 'iloveitaly_attachment_image_link_remove_filter');
function iloveitaly_attachment_image_link_remove_filter($content) {
	$content = preg_replace(
		array(
			// "/media" for the roots theme
			'{<a(.*?)(wp-att|wp-content/uploads|/media)[^>]*><img}',
			'{ wp-image-[0-9]*" /></a>}'
		),
		array(
			'<img',
			'" />'
		),
		$content
	);
	return $content;
}



/*
 * Search rewrites
 */

$searchPage = get_page_by_path("search");

function custom_rewrite_tag()
{
	add_rewrite_tag('%searcher%', '([^&]+)');
}
add_action('init', 'custom_rewrite_tag', 10, 0);

function custom_rewrite_basic()
{
	$searchPage = get_page_by_path("search");

	add_rewrite_rule('^search/([^/]*)/?', 'index.php?page_id='.$searchPage->ID.'&searcher=$matches[1]', 'top');
}
add_action('init', 'custom_rewrite_basic');

?>