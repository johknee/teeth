<?php

class TeethPage extends PostGeneric {

  /**
   * This method is a public method that tells
   * this instance of the object
   * that this is actually an ajax call for 
   * more items, so it overrides the standard
   * 'page' template with just the 'post-items'
   * template
   */
  public function setLoadMore() {
    $this->overrideTemplate("post-items");
    $this->loadMore = true;
    $this->set("loadMore", true);
  }

  /**
   * @private
   *
   * This method grabs all the necessary extras
   * needed to populate the home page
   */
  private function _homeMethods() {

    // If this is NOT an ajax call for more posts (see `setLoadMore()`) also do these
    if(!isset($this->loadMore)) {
      // Set the featured post
      $featured = TeethUtils::returnFirst($this->getPosts(array(
        "posts_per_page" => 1,
        "is_featured" => "yes",
      ), true));

      $featured->featuredImage = wp_get_attachment_image_src($featured->thumbnail, "featured");

      $this->set('featured', $featured);

      // If this is an ajax call for the page
      // you also need these not set header variables
      if(!$this->echo) {
        $this->set('header',(object) array(
          'bottom' => array(
            'items' => wp_get_nav_menu_items("Bottom")
          )
        ));
      }
    }

    $homeArgs = array(
      "paged" => $this->page
    );

    if(isset($featured)) {
      $homeArgs['post__not_in'] = [$featured->ID];
    }

    $homePosts = $this->getPosts($homeArgs, false);

    // Sets the posts to be displayed on the home page
    $this->set('homePosts', $homePosts);

    // If the amount of posts is greater than 12
    // then set the ability to display a load more
    $bool = (count($homePosts) <= 24) ? false : true;
    $this->set('morePosts', $bool);
  }

  /**
   * @private
   *
   * This method grabs all the necessary extras
   * needed to populate the about page
   */
  private function _aboutMethods() {
    // Grabs common methods between certain pages
    $this->_commonMethods();

    // The info boxes that display on the about page
    $this->set('infoFields', get_field("info_fields", $this->post->ID));
  }

  /**
   * @private
   *
   * This method grabs all the necessary extras
   * needed to populate the stockist page
   */
  private function _stockistsMethods() {
    // Grabs common methods between certain pages
    $this->_commonMethods();

    // Grab all of the stockists
    $stockists = $this->getPosts(array(
      "post_type"       => "stockist",
      "posts_per_page"  => -1,
      "orderby"         => "title",
      "order"           => "ASC",      
    ));

    // Add the information custom field to
    // each of their objects
    $stockists = array_map(function($s) {
      $s->information = get_field("address", $s->ID);
      $s->regions = wp_get_post_terms($s->ID,"region");
      return $s;
    }, $stockists);

    $this->set('stockists', $stockists);



    $locations = get_terms('region', array(
        'hide_empty' => true,
        'parent' => 0
    ));

    $sortedLocations = array();

    foreach($locations as $k => $v) {
      $curr = array();
      $curr['name'] = $v->name;
      $curr['data'] = get_terms('region', array(
        'hide_empty' => true,
        'parent' => $v->term_id
      ));

      $sortedLocations[] = $curr;
    }

    $this->set('locations', $sortedLocations);
  }

  /**
   * @private
   *
   * This method grabs all the necessary extras
   * needed to populate the advertise page
   */
  private function _advertiseMethods() {
    // Grabs common methods between certain pages
    $this->_commonMethods();
  }

  /**
   * @private
   *
   * This method grabs all the necessary extras
   * needed to populate the submit page
   */
  private function _submitMethods() {
    // Grabs common methods between certain pages
    $this->_commonMethods();
  }

  /**
   * @private
   *
   * This method grabs all the necessary extras
   * needed to populate the search page
   */
  private function _searchMethods() {
    $searchPosts = $this->getPosts(array(
      "paged"           => 1,
      "s"               => $this->args['search'],
      "posts_per_page"  => -1
    ), false);

    $this->set('searchPosts', $searchPosts);

    $this->set('search', $this->args['search']);

    if (count($searchPosts) === 0) {
      $this->set('noResults', true);
    }
  }

  /**
   * @private
   *
   * These are methods that are common to all of
   * the black pages
   */
  private function _commonMethods() {
    // The large headline type
    $this->set('largeType', get_field("large_text", $this->post->ID));

    // The nav that is set inline to these pages
    $this->set('inlineNav', wp_get_nav_menu_items("Page"));
  }

  public function init($echo = false, $args = []) {
    // Echo is true if this is an original page call, not ajax
    $this->echo = $echo;

    $this->args = $args;

    // Caches the post variable locally for the methods to access
    $this->post = $this->get("post");

    // Determines what methods need adding based on the post name
    if ($this->post) {

      switch($this->post->post_name) {
        case "home":
          // If a page argument is passed in use that, but default to 1
          $this->page = isset($args['page']) ? $args['page'] : 1;
          $this->_homeMethods();
          break;
        case "about":
          $this->_aboutMethods();
          break;
        case "stockists":
          $this->_stockistsMethods();
          break;
        case "advertise":
          $this->_advertiseMethods();
          break;
        case "submit":
          $this->_submitMethods();
          break;
        case "search":
          $this->_searchMethods();
          break;
      }

      // Apply the html filters to the post content
      $this->post->content = apply_filters("the_content", $this->post->post_content);

      // Re add the post object with its modified content
      $this->set('post', $this->post);

    }

    // If this is a load more call via ajax, pass in the necessary variables
    if(isset($this->loadMore) && $this->loadMore) {
      $this->set('posts', $this->get('homePosts'));
      $this->set('loadPage', $this->page);
    }
    
    // Call the render function in PostGeneric
    return $this->render($echo);
  }
}