<?php

class TeethPost extends PostGeneric {

  public function init($echo = false, $first = true) {

    $this->singlePost = $this->_extendPost();
    $this->set('post', $this->singlePost);

    if($first) {
      $this->set('sidebar', $this->_getSidebar());
    }

    return $this->render($echo);
  }

  private function _getSidebar() {
    $post = $this->get('post');

    $sidebar = $this->getPosts(array(
      "post__not_in" => array($post->ID),
      "post_status"  => "publish",
      "tax_query"    => array(
                        array(
                          "taxonomy"  => "category",
                          "field"     => "term_id",
                          "terms"     => $post->single_category->term_id, // Where term_id of Term 1 is "1".
                        ) 
      )
    ), false);

    array_unshift($sidebar, $post);

    return $sidebar;
  }

  public function setSingle() {
    $this->overrideTemplate("single-post");
  }

  private function _extendPost() {
    $post = $this->get('post');

    $post->thumbnail = get_post_thumbnail_id($post->ID);
    $post->image = wp_get_attachment_image_src($post->thumbnail, "large");
    $post->category = get_the_category($post->ID);
    $post->single_category = TeethUtils::returnFirst($post->category);
    $post->information = get_field("additional_info", $post->ID);
    $post->content = apply_filters("the_content", $post->post_content);
    $post->embed = get_field("video_embed", $post->ID);

    $post->social = $this->_socialDetails($post);

    return $post;
  }

  private function _socialDetails($post) {
    $details = [
      'twitter'   => $this->_params(
        'https://twitter.com/intent/tweet', [
        'text'    => $post->post_title,
        'url'     => get_permalink($post->ID),
        'via'     => 'teeth_mag'
      ]),

      'facebook'  => $this->_params(
        'http://www.facebook.com/sharer.php'
      ),

      'pinterest'  => $this->_params(
        'http://www.pinterest.com'
      )

    ];

    return $details;
  }

  private function _params($url, $arr = array()) {
    $build = $url;

    if(isset($arr)) {
      return $build  . "?" . http_build_query($arr);
    }
    else {
      return $build;
    }
  }
}