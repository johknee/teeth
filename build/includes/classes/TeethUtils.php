<?php

class TeethUtils {
  static public function returnState($post = false, $env = array(), $template = '', $echo = false) {
    $state = [];
      
    if(!$post) {
      $state['id'] = '0';
      $state['title'] = "Not found";
      $state['absolute'] = $env->home."/404";
      $state['slug'] = '404';
      $state['type'] = 'error';
      $state['safe'] = '404';
      return (object) $state;
    }
    else {
      switch($template) {
        case "category":
          $state['id'] = $post->term_id;
          $state['title'] = $post->name;
          $state['absolute'] = get_category_link($post);
          $state['slug'] = self::minusRoot($env->home, $state['absolute']);
          $state['type'] = 'category';
          $state['safe'] = str_replace('/','',$state['slug']);
          break;
        case "404":
          $state['id'] = '0';
          $state['title'] = "Not found";
          $state['absolute'] = $env->home."/404";
          $state['slug'] = '404';
          $state['type'] = 'error';
          $state['safe'] = '404';
          break;
        case "nav":
          $state['id'] = $post->object_id;
          $state['title'] = $post->title;
          $state['absolute'] = $post->url;
          $state['slug'] = self::minusRoot($env->home, $state['absolute']);
          $state['type'] = $post->object;
          $state['safe'] = sanitize_title(str_replace('/','',$state['slug']));
          break;
        default:
          $state['id'] = $post->ID;
          $state['title'] = $post->post_title;
          $state['absolute'] = get_permalink($post);
          $state['slug'] = self::minusRoot($env->home, $state['absolute']);
          $state['type'] = $post->post_type;
          $state['safe'] = $post->post_name;
          break;
      }

      if(!$echo) {
        return (object) $state;
      }
      else {
        return $this->stateFormat($state);
      }
    }
  }

  static public function stateFormat($state) {
    $str = 'href="'.$state->absolute.'" data-ajax data-state=\''.json_encode($state,JSON_HEX_APOS).'\'';

    return $str;
  }

  static public function twigState($post, $env, $template) {
    return self::stateFormat(self::returnState($post, $env, $template));
  }

  static public function minusRoot($home, $str) {
    return str_replace($home, "", $str);
  }

  static public function returnFirst($arr) {
    if(count($arr)) {
      return $arr[0];
    }
    else {
      return false;
    }
  }
}