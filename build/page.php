<?php
$obj = new TeethPage($post);

/*

The object `TeethPage` gets passed in the global $post object
from which it gets initialised in the SUPER class "PostGeneric.php"

PostGeneric does a lot of the heavy lifting and working out of things,
including getting together the header and the footer elements.

The object HAS to be initialiased as $obj because in the header and footer
they reference the global `$obj` variable to create the necessary parts.

*/

get_header();
$obj->init(true);
get_footer();
?>