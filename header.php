<?php
global $obj;
?>
<!DOCTYPE html>
<html class="<?php echo $obj->getPart('htmlClass');?>" data-type="<?php echo $post->post_type ?>">
<head>
    <?php
    echo $obj->getPart('meta');
    ?>

    <?php
    wp_head();
    ?>
</head>

<body>

    <!--[if lte IE 8]>
    <div id="ie-overlay">
        <table cellpadding="0" cellspacing="0" border="0">
            <tr>
                <td>
                    <h2>Your browser is outdated</h2>
                    <p>
                        Unfortunately the browser you are using is outdated and you are unable to view our site with it. For a better internet experience please update to the latest version, or even try <a href="https://www.google.com/chrome/browser/desktop/index.html">Google Chrome</a> or <a href="https://www.mozilla.org/en-GB/firefox/new/">Mozilla Firefox</a>
                    </p>
                </td>
            </tr>
        </table>
    </div>
    <![endif]-->

    <?php

    include_once(get_template_directory()."/assets/images/svg-defs.svg");    

    echo $obj->getPart('header');

    ?>