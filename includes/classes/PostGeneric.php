<?php 

class PostGeneric {
  private $post;
  private $data = array();
  
  function __construct($post) {
    // Caching the post variable passed into the object
    $this->post = $post;

    // Adds the post into the data array, so that its accessible
    // to the twig template at the end
    $this->data['post'] = $this->post;

    // Registers all `environment variables`
    $this->_registerEnvironment();

    // Registers all necessary dependencies for the class
    $this->_registerDependencies();

    // Gets the home page post on every call, because it stores
    // various global fields (like the nav tagline etc.)
    $this->_getHome();
    $this->_getSearch();

    // Determines the template to display
    $this->_loadTemplate($this->post);

    // Creates the state of this current object to be used in subsequent calls
    $this->state = TeethUtils::returnState($this->post, $this->env, $this->templateName);

    // Registers any structure variables necessary
    $this->_registerStructure();
  }

  /**
   * @private
   *
   * This method grabs environment variables, that are
   * necessary for a lot of further methods down the line
   */
  private function _registerEnvironment() {
    $env = [];

    $env['theme'] = get_template_directory();
    $env['theme_uri'] = get_template_directory_uri();
    $env['templates'] = $env['theme']."/includes/template";
    $env['home'] = get_home_url();
    $env['ajax'] = admin_url('admin-ajax.php');
    $env['nonce'] = wp_create_nonce("ajaxHandlerNonce");

    $this->env = (object) $env;
  }

  /**
   * @private
   *
   * Methods that are 
   */
  private function _registerDependencies() {
    Twig_Autoloader::register();

    $this->loader = new Twig_Loader_Filesystem($this->env->templates);
    $this->twig = new Twig_Environment(
      $this->loader, array(
        'debug' => true,
        //'cache' => '/path/to/compilation_cache',
      )
    );
    $this->_registerExtensions();

    //enable debugging
    $this->twig->addExtension(new Twig_Extension_Debug());    
  }

  private function _getHome() {
    if (isset($post) && $this->post->post_title === 'Home') {
      $this->home = $this->post;
    }
    else {
      $this->home = get_page_by_path("home");
    }

    $this->home->tagline = TeethUtils::returnFirst(get_field("nav_tagline", $this->home->ID));
    $this->home->splash = get_field("splash_screen", $this->home->ID);

    if ($this->home->splash) {
      $this->home->splashContent = [
        "text"    => get_field("splash_text", $this->home->ID),
        "image"   => get_field("splash_image", $this->home->ID),
        "link"   => get_field("splash_link", $this->home->ID)
      ];
    }
  }

  private function _getSearch() {
    $this->search = get_page_by_path("search");
  }

  private function _registerExtensions() {
    $functions = array();
    $functions['state'] = new Twig_SimpleFunction("state", function ($post, $env, $template) {
      return TeethUtils::twigState($post, $env, $template);
    },
    array(
      'is_safe' => array( 'html' )
    ));
    $functions['actor'] = new Twig_SimpleFunction("actor", function ($echo) {
      return $echo ? 'actor' : 'actor trans--in';
    },
    array(
      'is_safe' => array( 'html' )
    ));


    $functions['lazyImage'] = new Twig_SimpleFunction("lazyImage", function ($url, $wid, $hei) {
      $color = 'eee';

      $html = '<div class="lazy-image" data-url="'.$url.'">' . 
                '<img class="lazy-image__image" src="http://dummyimage.com/'.$wid.'x'.$hei.'/'.$color.'/'.$color.'">'.
              '</div>';

      return $html;
    },
    array(
      'is_safe' => array( 'html' )
    ));

    foreach($functions as $v) {
      $this->twig->addFunction($v);
    }
  }

  private function _registerStructure() {
    $structure = [];

    $structure['name'] = get_bloginfo("name");
    $structure['title'] = $this->state->title .' — '. $structure['name'];
    $structure['dark'] = $this->_determineDark($this->post);
    $structure['active'] = TeethHeader::determineActive($this->state);
    $structure['adverts'] = TeethAdvert::anyPosts();

    $this->structure = (object) $structure;
  }

  private function _determineDark($post) {
    if ($post) {
      switch($post->post_name) {
        case "about":
        case "advertise":
        case "stockists":
        case "submit":
          return true;
          break;
        default:
          return false;
          break;
      }
    }
  }

  private function _loadTemplate($post) {
    if($post) {
      if($post->post_name && file_exists($this->env->templates."/".$post->post_type."-".$post->post_name.".html")) {
        $tmpl = $post->post_type."-".$post->post_name;
      }
      else if(file_exists($this->env->templates."/".$post->post_type.".html")) {
        $tmpl = $post->post_type;
      }
      else if(isset($post->taxonomy) && $post->taxonomy === 'category') {
        $tmpl = 'category';
      }
      else {
        $tmpl = "404";
      }
    }
    else {
      $tmpl = "404";
    }

    $this->templateName = $tmpl;
    $this->template = $this->twig->loadTemplate($tmpl.'.html');
    $this->env->template = $tmpl;
  }

  public function overrideTemplate($tmpl) {
    $this->templateName = $tmpl;
    $this->template = $this->twig->loadTemplate($tmpl.'.html');
    $this->env->template = $tmpl;
  }

  private function _getArgs() {
    $defaults = array(
      "env" => $this->env,
      "structure" => $this->structure,
      "state" => TeethUtils::returnState($this->post, $this->env, $this->templateName),
      "searchState" => TeethUtils::returnState($this->search, $this->env, $this->templateName),
      "home" => $this->home
    );
    
    return array_merge($defaults, $this->data);
  }

  public function set($key, $data) {
    $this->data[$key] = $data;
  }

  public function get($key) {
    return ($key == 'all') ? $this->data : $this->data[$key];
  }

  public function getPart($name) {
    if(!isset($this->data[$name])) {

      switch($name) {
        case "header":
          $header = new TeethHeader($this->_getArgs());
          $data = $header->getData();
          break;
        case "htmlClass":
          $string = array();

          $string[] = $this->structure->dark ? "site--dark" : "";

          $string = implode(" ", $string);
          break;
        default:
          $data = false;
          break;
      }

      if(isset($data)) {
        $this->data[$name] = $data;
      }
    }

    if(isset($string)) {
      echo $string;
    }
    else {
      $template = $this->twig->loadTemplate($name.".html");
      return $template->render($this->_getArgs());
    }
  }

  public function getPosts($args = array(), $full = false) {
    $defaults = array(
      "post_type"       => "post",
      "paged"           => 1,
      "posts_per_page"  => 24,
      "post_status"     => "publish"
    );

    $arguments = array_merge($defaults, $args);

    $query = new WP_Query($arguments);

    $posts = array();

    foreach($query->posts as $k => $p) {
      $c = $p;

      switch($c->post_type) {
        case "post":
          $c->single_category = TeethUtils::returnFirst(get_the_category($p->ID));
          $c->thumbnail = get_post_thumbnail_id($c->ID);
          $c->image = wp_get_attachment_image_src($c->thumbnail, "large");

          if($full) {
            $c->information = get_field("additional_info", $c->ID);
          }
          break;
      }

      $posts[] = $c;
    }

    return $posts;
  }

  public function render($echo = false) {
    $args = $this->_getArgs();
    $args['echo'] = $echo;

    if($echo) {
      echo $this->template->render($args);
    }
    else {
      $obj = (object) array(
        "arguments" => $args,
        "content" => $this->template->render($args)
      );

      return $obj;
    }
  }
}