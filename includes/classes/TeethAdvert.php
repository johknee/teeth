<?php 

class TeethAdvert {

	function __construct($size, $placement = 0, $id = false) {
		$this->size = $size;
		$this->placement = $placement;
		$this->post = $this->_getAdvert($id);
		$this->_dependencies();
	}

	private function _getAdvert($id) {
		if($id) {
			$post = get_post($id);
		}
		else {
		    $defaults = array(
				"post_type"      => 'advert',
				'orderby'        => 'rand',
				'posts_per_page' => 1,
				'meta_key'		 => 'advert_placement',
				'meta_value'	 => $this->placement
		    );

		    $posts = get_posts($defaults);
		    $post = $posts[0];
		}

		if($post) {
		    $post->thumbnail = get_post_thumbnail_id($post->ID);
		    $post->image = wp_get_attachment_image_src($post->thumbnail, "large");
		    $post->video = get_field("advert_video", $post->ID);
		    $post->link = get_field("advert_link", $post->ID);
		    $post->content = apply_filters("the_content", $post->post_content);
		    $post->position = get_field("advert_content_position", $post->ID);
		    $post->placement = $this->placement;
		}

		return $post;
	}

	private function _dependencies() {
		if($this->post) {
		    Twig_Autoloader::register();

		    $this->loader = new Twig_Loader_Filesystem(get_template_directory()."/includes/template");
		    $this->twig = new Twig_Environment($this->loader, array(
		        //'cache' => '/path/to/compilation_cache',
		    ));
		    $this->template = $this->twig->loadTemplate('advert-content.html');
		}
	}

	public function render() {

		if($this->post) {
			$args = [
				"post" 			=> $this->post,
				"size" 			=> $this->size,
			];

			$obj = array();
			$obj['args'] 		= $args;
			$obj['content'] 	= $this->template->render($args);
		}
		else {
			$obj = array();
			$obj['noAdvert'] 	= true;
		}

		return $obj;
	}

	static public function anyPosts() {
		$defaults = array(
			"post_type"      => "advert",
			'orderby'        => 'rand',
			'posts_per_page' => 1,
		    "post_status"     => "publish"
	    );

	    $posts = get_posts($defaults);

	    return count($posts) ? true : false;
	}
}