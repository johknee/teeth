<?php

class TeethCategory extends PostGeneric {

  public function setLoadMore() {
    $this->overrideTemplate("post-items");
    $this->loadMore = true;
  }

  private function _categoryMethods() {
    $this->set('categoryPosts', $this->getPosts(array(
      "paged"        => $this->page,
      "cat"          => $this->post->term_id,
      "post_status"  => "publish",
    ), false));

    $bool = (count($this->get('categoryPosts')) < 12) ? false : true;
    $this->set('morePosts', $bool);
  }

  public function init($echo = false, $args = []) {
    $this->post = $this->get('post');
    $this->page = isset($args['page']) ? $args['page'] : 1;

    $this->_categoryMethods();

    if(isset($this->loadMore) && $this->loadMore) {
      $this->set('posts', $this->get('categoryPosts'));
      $this->set('loadPage', $this->page);
    }
    
    return $this->render($echo);
  }
}