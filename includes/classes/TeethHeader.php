<?php 

class TeethHeader {

  function __construct($args) {
    $this->args = $args;

    $this->_setNav();
  }

  private function _setNav() {
    $this->top = (object) array(
      "items" => wp_get_nav_menu_items("Main"),
      "tagline" => $this->args['home']->tagline
    );
    $this->bottom = (object)  array(
      "items" => wp_get_nav_menu_items("Bottom")
    );
    $this->active = $this->args['structure']->active;
  }

  static public function determineActive($state) {
    if($state) {
      $type = $state->type;
      $name = $state->title;

      switch($type) {
        case "page":
          switch($name) {
            case "Home":
            default:
              return $state->id;
              break;
          }
          break;
        case "category":
          return $state->id;
          break;
        default:
          return false;
          break;
      }
    }
    else {
      return false;
    }
  }

  public function getData() {
    return (object) array(
      "top" => $this->top,
      "bottom" => $this->bottom,
      "active" => $this->active
    );
  }
}