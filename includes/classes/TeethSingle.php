<?php

class TeethPost extends PostGeneric {

  public function init($echo = false, $first = true) {
    if($first) {
      $this->set('sidebar', $this->getPosts(array(
        "posts_per_page" => 30,
        "post_status"    => "publish"        
      ), false));
    }

    $this->set('post', $this->_extendPost());

    return $this->render($echo);
  }

  private function _extendPost() {
    $post = $this->get('post');

    $post->thumbnail = get_post_thumbnail_id($post->ID);
    $post->image = wp_get_attachment_image_src($post->thumbnail, "large");
    $post->category = get_the_category($post->ID);
    $post->single_category = TeethUtils::returnFirst($post->category);
    $post->information = get_field("additional_info", $post->ID);
    $post->content = apply_filters("the_content", $post->post_content);

    return $post;
  }
}